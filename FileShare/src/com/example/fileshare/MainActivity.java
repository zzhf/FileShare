package com.example.fileshare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Button fasong=null;
	private Button jieshou=null;
	private Button pcclient=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //��ʼ����ť
        fasong=(Button)findViewById(R.id.mainFasong);
        jieshou=(Button)findViewById(R.id.mainJieshou);
        pcclient=(Button)findViewById(R.id.pcclient);
        //�󶨼�����
        fasong.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				Intent intent=new Intent(MainActivity.this,FasongActivity.class);
				startActivity(intent);
			}
		});
        jieshou.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				Intent intent=new Intent(MainActivity.this,JieshouActivity.class);
				startActivity(intent);
			}
		});
        pcclient.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				Intent intent=new Intent(MainActivity.this,PCActivity.class);
				startActivity(intent);
			}
		});
    }
}
