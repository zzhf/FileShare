package com.example.fileshare;

import java.lang.reflect.Method;

import android.app.Activity;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class JieshouActivity extends Activity {
	private TextView result;
	private ListView reslist;
	private Button fanhui,zhunbei;
	private WifiManager wifiMng;
	private boolean state=false;
	private ServerTerminal server;
	public static Handler hand;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jieshou);
		//初始化
		wifiMng=(WifiManager) getSystemService(JieshouActivity.WIFI_SERVICE);
		//标签
		result=(TextView)findViewById(R.id.jieshouResult);
		//列表
		reslist=(ListView)findViewById(R.id.jieshouReslist);
		//按钮
		fanhui=(Button)findViewById(R.id.jieshouFhbtn);
		zhunbei=(Button)findViewById(R.id.jieshouZbbtn);
		//为按钮添加监听器
		hand=new Handler(){
			public void handleMessage(Message msg) {
				switch(msg.what){
				case 0:
					Bundle data=msg.getData();
					ArrayAdapter<String> adpter=new ArrayAdapter<>(JieshouActivity.this, R.layout.default_item,data.getStringArrayList("names"));
					reslist.setAdapter(adpter);
					break;
				case 1:
					Bundle jindu=msg.getData();
					result.setText(jindu.getString("wenzi")+Math.round(jindu.getFloat("prges")*100)+"%");
					break;
				default:
					break;
				}
			}
		};
		fanhui.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				finish();
			}
		});
		zhunbei.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				state=!state;
				wifiMng.setWifiEnabled(false);
				String ssid="快如狗"+(int)(Math.random()*10000);
				setWifiAp(ssid, "0f0484250", state);
				if(state){
					server=new ServerTerminal(Environment.getExternalStorageDirectory().getAbsolutePath());
					server.start();
					zhunbei.setText("关闭热点:"+ssid);
				}else{
					server.stopServer();
					zhunbei.setText("建立热点");
				}
			}
		});
	}
	public void setWifiAp(String mSSID,String mPasswd,boolean state){
		Toast.makeText(JieshouActivity.this, "正在"+(state?"打开":"关闭")+"热点", Toast.LENGTH_SHORT).show();
		if (wifiMng.isWifiEnabled()) {
			wifiMng.setWifiEnabled(false);
        }
        try {
            WifiConfiguration apConfiguration = new WifiConfiguration();
            apConfiguration.SSID = mSSID;
            apConfiguration.preSharedKey = mPasswd;
            apConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            Method method = wifiMng.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifiMng, apConfiguration, state);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
