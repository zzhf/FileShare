package com.example.fileshare;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PCActivity extends Activity {
	private TextView resultlabel,recvlabel;
	private ListView infolist;
	private Button fasong, xuanze, createap;
	private PCServer server;
	private boolean state = false;
	private WifiManager wifiMng;
	private ArrayList<File> selectedFile;
	public static Handler hand;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pc);
		wifiMng = (WifiManager) getSystemService(PCActivity.WIFI_SERVICE);
		resultlabel = (TextView) findViewById(R.id.resultlabel);
		recvlabel =(TextView) findViewById(R.id.recvtv);
		infolist = (ListView) findViewById(R.id.infolist);
		fasong = (Button) findViewById(R.id.fasong);
		xuanze = (Button) findViewById(R.id.xuanze);
		createap = (Button) findViewById(R.id.createap);
		selectedFile=new ArrayList<File>();
		// 创建热点按钮时间监听器
		createap.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				state = !state;
				wifiMng.setWifiEnabled(false);
				String ssid="快如狗"+(int)(Math.random()*10000);
				setWifiAp(ssid, "0f0484250", state);
				if (state) {
					server = new PCServer();
					server.start();
					createap.setText("关闭热点:"+ssid);
				} else {
					server.stopServer();
					createap.setText("创建热点");
				}
			}
		});
		// 选择文件按钮时间监听器
		xuanze.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent intent = new Intent(PCActivity.this,
						XuanzeActivity.class);
				startActivityForResult(intent, 0);
			}
		});
		// 发送按钮时间监听器
		fasong.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				if (selectedFile.size() == 0) {
					Toast.makeText(PCActivity.this, "未选中任何文件",
							Toast.LENGTH_SHORT).show();
				} else {
					new PCSend(selectedFile).start();
				}
			}
		});
		// 进度显示处理
		hand = new Handler() {
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					Bundle data = msg.getData();
					resultlabel.setText(data.getString("jindu")
							+ Math.round((data.getFloat("prges") * 10000.0))
							/ 100 + "%");
					break;
				case 1:
					Bundle data2 = msg.getData();
					resultlabel.setText(data2.getString("jindu")
							+ Math.round((data2.getFloat("prges") * 10000.0))
							/ 100 + "%");
					break;
				default:
					System.out.println("收到未知消息");
					break;
				}
			}
		};
		Intent intent = getIntent();
		String action=intent.getAction();
		String type=intent.getType();
		if(action!=null && type!=null){
			Bundle data=intent.getExtras();
			System.out.println("收到的action:"+action);
			switch(action){
			case Intent.ACTION_SEND:
				System.out.println("收到的type:"+type);
				switch(type){
				case "image/*":
					Uri imgcontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("image分享收到的Uri:"+imgcontent.toString());
					String imgpath=getRealPathFromURI(imgcontent);
					if(imgpath==null){
						Toast.makeText(this, "图片文件路径未找到", Toast.LENGTH_SHORT).show();
						break;
					}
					File img=new File(imgpath);
					selectedFile.add(img);
					setFileList();
					break;
				case "video/*":
					Uri videocontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("video分享收到的Uri:"+videocontent.toString());
					String videopath=getRealPathFromURI(videocontent);
					if(videopath==null){
						Toast.makeText(this, "视频文件路径未找到", Toast.LENGTH_SHORT).show();
						break;
					}
					File video=new File(videopath);
					selectedFile.add(video);
					setFileList();
					break;
				case "application/vnd.android.package-archive":
					Uri apkcontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("apk分享收到的Uri:"+apkcontent.toString());
					File apk=new File(apkcontent.getEncodedPath());
					if(apk.exists()){
						selectedFile.add(apk);
						setFileList();
						Toast.makeText(PCActivity.this, "应用文件已添加",Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(PCActivity.this, "应用文件未找到", Toast.LENGTH_SHORT).show();
					}
					break;
				}
				break;
			case Intent.ACTION_SEND_MULTIPLE:
				System.out.println("多文件收到的type:"+type);
				switch(type){
				case "image/*":
					System.out.println("进入多图片处理");
					ArrayList<Uri> mimgs=data.getParcelableArrayList(Intent.EXTRA_STREAM);
					if(mimgs==null){
						Toast.makeText(PCActivity.this, "多图片处理未获取Uri", Toast.LENGTH_SHORT).show();
						break;
					}
					for(int i=0;i<mimgs.size();i++){
						Uri tmp=mimgs.get(i);
						System.out.println("图片Uri:"+tmp.toString());
						File img=new File(getRealPathFromURI(tmp));
						if(img.exists()){
							selectedFile.add(img);
						}else{
							System.out.println("图片文件未找到");
						}
					}
					setFileList();
					break;
				case "video/*":
					System.out.println("进入多视频处理");
					ArrayList<Uri> mvideos=data.getParcelableArrayList(Intent.EXTRA_STREAM);
					if(mvideos==null){
						Toast.makeText(PCActivity.this, "多视频处理未获取Uri", Toast.LENGTH_SHORT).show();
						break;
					}
					for(int i=0;i<mvideos.size();i++){
						Uri tmp=mvideos.get(i);
						System.out.println("视频Uri:"+tmp.toString());
						File img=new File(getRealPathFromURI(tmp));
						if(img.exists()){
							selectedFile.add(img);
						}else{
							System.out.println("视频文件未找到");
						}
					}
					setFileList();
					break;
				}
				break;
			}
		}
	}
	private String getRealPathFromURI(Uri contentUri) {  
        String[] proj = {MediaStore.MediaColumns.DATA};  
        Cursor cursor=getContentResolver().query(contentUri, proj, null, null, null);  
        if(cursor.moveToNext()){  
            return cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));  
        }  
        cursor.close();  
        return null;  
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == 0) {
			// 处理文件选择界面传回的消息
			Bundle shuju = data.getExtras();
			ArrayList<File> flsttmp=(ArrayList<File>) shuju
					.getSerializable("selectedfile");
			if(flsttmp.size()>0){
				selectedFile =flsttmp;
				setFileList();
			}else{
				Toast.makeText(PCActivity.this, "未选择任何文件", Toast.LENGTH_SHORT).show();
			}
		}
	}
	//设置文件列表
	private void setFileList() {
		ArrayList<String> selectedfilenames = new ArrayList<String>();
		for (int i = 0; i < selectedFile.size(); i++) {
			selectedfilenames.add(selectedFile.get(i).getName());
		}
		ArrayAdapter<String> aryadpt = new ArrayAdapter<String>(
				PCActivity.this, R.layout.default_item, selectedfilenames);
		infolist.setAdapter(aryadpt);
	}

	public void setWifiAp(String mSSID, String mPasswd, boolean state) {
		Toast.makeText(PCActivity.this, "正在" + (state ? "打开" : "关闭") + "热点",
				Toast.LENGTH_SHORT).show();
		if (wifiMng.isWifiEnabled()) {
			wifiMng.setWifiEnabled(false);
		}
		try {
			WifiConfiguration apConfiguration = new WifiConfiguration();
			apConfiguration.SSID = mSSID;
			apConfiguration.preSharedKey = mPasswd;
			apConfiguration.allowedKeyManagement
					.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			Method method = wifiMng.getClass().getMethod("setWifiApEnabled",
					WifiConfiguration.class, boolean.class);
			method.invoke(wifiMng, apConfiguration, state);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
