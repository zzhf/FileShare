package com.example.fileshare;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FasongActivity extends Activity {
	private TextView macnames, result;
	private ListView maclist, resultlist;
	private Button fasong, xuanze, saomiao, fanhui;
	private ArrayList<File> selectedFile;
	private WifiManager wifiMng;
	private ArrayList<String> ssids;
	private BroadcastReceiver brwifi,wificonctbr;
	private Thread client;
	public static Handler hand;
	public static int buffersize=1024*2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fasong);
		// 动态权限检查及申请

		// 初始化
		wifiMng = (WifiManager) getSystemService(FasongActivity.WIFI_SERVICE);
		ssids = new ArrayList<String>();
		selectedFile = new ArrayList<File>();
		hand = new Handler() {
			public void handleMessage(android.os.Message msg) {
				switch (msg.what) {
				case 0:
					Bundle data = msg.getData();
					result.setText("文件："
							+ data.getString("jindu")
							+ (float) (Math.round(data.getFloat("prges") * 10000.0))
							/ 100.0 + "%");
					break;
				default:
					Toast.makeText(FasongActivity.this, "收到未知消息",
							Toast.LENGTH_SHORT).show();
				}
			};
		};
		// 标签
		macnames = (TextView) findViewById(R.id.fasongMacnames);
		result = (TextView) findViewById(R.id.fasongResult);
		// 列表
		maclist = (ListView) findViewById(R.id.fasongMaclist);
		resultlist = (ListView) findViewById(R.id.fasongReslist);
		// 按钮
		fasong = (Button) findViewById(R.id.fasongFsbtn);
		xuanze = (Button) findViewById(R.id.fasongXzbtn);
		saomiao = (Button) findViewById(R.id.fasongSmbtn);
		if (!wifiMng.isWifiEnabled()) {
			saomiao.setText("打开无线");
		}
		fanhui = (Button) findViewById(R.id.fasongFhbtn);
		// 初始化广播
		brwifi = new BroadcastReceiver() {

			public void onReceive(Context arg0, Intent arg1) {
				final String action = arg1.getAction();
				if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
					List<ScanResult> reslist = wifiMng.getScanResults();
					ssids.clear();
					for (int i = 0; i < reslist.size(); i++) {
						if ((!ssids.contains(reslist.get(i).SSID.trim()))
								&& reslist.get(i).SSID.contains("快如狗")) {
							ssids.add(reslist.get(i).SSID);
						}
					}
					ArrayAdapter<String> wifinames = new ArrayAdapter<String>(
							FasongActivity.this, R.layout.default_item, ssids);
					maclist.setAdapter(wifinames);
				}
			}
		};
		wificonctbr=new BroadcastReceiver(){
			public void onReceive(Context arg0, Intent arg1) {
				if(WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(arg1.getAction())){
					Parcelable parcelableExtra = arg1.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
		            if (parcelableExtra != null){
		                // 获取联网状态的NetWorkInfo对象
		                NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;
		                //获取的State对象则代表着连接成功与否等状态
		                NetworkInfo.State state = networkInfo.getState();
		                //判断网络是否已经连接
		                boolean isConnected = state == NetworkInfo.State.CONNECTED;
		                if (isConnected) {
		                	String ssid=wifiMng.getConnectionInfo().getSSID();
		                	if(ssid.contains("快如狗")){
		                		macnames.setText(ssid+"(可发送)传输时勿扫描");
		                	}else{
		                		macnames.setText("连接错误(不可发送)，点击重连");
		                	}
		                } else {
		                	macnames.setText("连接已断开");
		                }
		            }
				}
			}
			
		};
		// 注册无线扫描结果广播
		IntentFilter ittfilter = new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
		registerReceiver(brwifi, ittfilter);
		//注册无线连接改变广播
		IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(wificonctbr, filter);
		// 添加按钮监听器
		fasong.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				client = new Thread(new ClientTerminal(selectedFile));
				client.start();
			}
		});
		xuanze.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent intent = new Intent(FasongActivity.this,
						XuanzeActivity.class);
				startActivityForResult(intent, 0);
			}
		});
		saomiao.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				if (wifiMng.isWifiEnabled()) {
					// 无线已经开启
					// 获取无线扫描结果
					wifiMng.startScan();
				} else {
					// 无线未开启
					if (getWifiApState() != WIFI_AP_STATE.WIFI_AP_STATE_DISABLED) {
						setWifiAp("热点已关闭", "0f0484250", false);
					}
					wifiMng.setWifiEnabled(true);
					while (!wifiMng.isWifiEnabled()) {
					}
					saomiao.setText("搜索机器");
					Toast.makeText(FasongActivity.this, "无线已开启",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		fanhui.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
		});
		maclist.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				System.out.println("连接目标机器");
				WifiConfiguration wificonfig = CreateWifiInfo(ssids.get(arg2),
						"0f0484250", 3);
				int wfid = wifiMng.addNetwork(wificonfig);
				wifiMng.enableNetwork(wfid, true);
			}
		});
		//响应系统分享功能
		Intent intent = getIntent();
		String action=intent.getAction();
		String type=intent.getType();
		if(action!=null && type!=null){
			Bundle data=intent.getExtras();
			System.out.println("收到的action:"+action);
			switch(action){
			case Intent.ACTION_SEND:
				System.out.println("收到的type:"+type);
				switch(type){
				case "image/*":
					Uri imgcontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("image分享收到的Uri:"+imgcontent.toString());
					String imgpath=getRealPathFromURI(imgcontent);
					if(imgpath==null){
						Toast.makeText(this, "图片文件路径未找到", Toast.LENGTH_SHORT).show();
						break;
					}
					File img=new File(imgpath);
					selectedFile.add(img);
					setFileList();
					break;
				case "video/*":
					Uri videocontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("video分享收到的Uri:"+videocontent.toString());
					String videopath=getRealPathFromURI(videocontent);
					if(videopath==null){
						Toast.makeText(this, "视频文件路径未找到", Toast.LENGTH_SHORT).show();
						break;
					}
					File video=new File(videopath);
					selectedFile.add(video);
					setFileList();
					break;
				case "application/vnd.android.package-archive":
					Uri apkcontent=data.getParcelable(Intent.EXTRA_STREAM);
					System.out.println("apk分享收到的Uri:"+apkcontent.toString());
					File apk=new File(apkcontent.getEncodedPath());
					if(apk.exists()){
						selectedFile.add(apk);
						setFileList();
						Toast.makeText(FasongActivity.this, "应用文件已添加",Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(FasongActivity.this, "应用文件未找到", Toast.LENGTH_SHORT).show();
					}
					break;
				}
				break;
			case Intent.ACTION_SEND_MULTIPLE:
				System.out.println("多文件收到的type:"+type);
				switch(type){
				case "image/*":
					System.out.println("进入多图片处理");
					ArrayList<Uri> mimgs=data.getParcelableArrayList(Intent.EXTRA_STREAM);
					if(mimgs==null){
						Toast.makeText(FasongActivity.this, "多图片处理未获取Uri", Toast.LENGTH_SHORT).show();
						break;
					}
					for(int i=0;i<mimgs.size();i++){
						Uri tmp=mimgs.get(i);
						System.out.println("图片Uri:"+tmp.toString());
						File img=new File(getRealPathFromURI(tmp));
						if(img.exists()){
							selectedFile.add(img);
						}else{
							System.out.println("图片文件未找到");
						}
					}
					setFileList();
					break;
				case "video/*":
					System.out.println("进入多视频处理");
					ArrayList<Uri> mvideos=data.getParcelableArrayList(Intent.EXTRA_STREAM);
					if(mvideos==null){
						Toast.makeText(FasongActivity.this, "多视频处理未获取Uri", Toast.LENGTH_SHORT).show();
						break;
					}
					for(int i=0;i<mvideos.size();i++){
						Uri tmp=mvideos.get(i);
						System.out.println("视频Uri:"+tmp.toString());
						File img=new File(getRealPathFromURI(tmp));
						if(img.exists()){
							selectedFile.add(img);
						}else{
							System.out.println("视频文件未找到");
						}
					}
					setFileList();
					break;
				}
				break;
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == 0) {
			// 处理文件选择界面传回的消息
			Bundle shuju = data.getExtras();
			ArrayList<File> flsttmp=(ArrayList<File>) shuju
					.getSerializable("selectedfile");
			if(flsttmp.size()>0){
				selectedFile =flsttmp;
				setFileList();
			}else{
				Toast.makeText(FasongActivity.this, "未选择任何文件", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void setFileList() {
		ArrayList<String> selectedfilenames = new ArrayList<String>();
		for (int i = 0; i < selectedFile.size(); i++) {
			selectedfilenames.add(selectedFile.get(i).getName());
		}
		ArrayAdapter<String> aryadpt = new ArrayAdapter<String>(
				FasongActivity.this, R.layout.default_item, selectedfilenames);
		resultlist.setAdapter(aryadpt);
	}

	// 创建无线连接配置
	public WifiConfiguration CreateWifiInfo(String SSID, String Password,
			int Type) {
		WifiConfiguration config = new WifiConfiguration();
		config.allowedAuthAlgorithms.clear();
		config.allowedGroupCiphers.clear();
		config.allowedKeyManagement.clear();
		config.allowedPairwiseCiphers.clear();
		config.allowedProtocols.clear();
		config.SSID = "\"" + SSID + "\"";
		config.priority = 1;
		// 清除已保存的快如狗无线配置信息
		List<WifiConfiguration> oldwifi = wifiMng.getConfiguredNetworks();
		for (int i = 0; i < oldwifi.size(); i++) {
			WifiConfiguration wifitmp = oldwifi.get(i);
			if (wifitmp.SSID.contains("快如狗")) {
				wifiMng.removeNetwork(wifitmp.networkId);
			}
		}
		if (Type == 1) // WIFICIPHER_NOPASS
		{
			config.wepKeys[0] = "";
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			config.wepTxKeyIndex = 0;
		}
		if (Type == 2) // WIFICIPHER_WEP
		{
			config.hiddenSSID = true;
			config.wepKeys[0] = "\"" + Password + "\"";
			config.allowedAuthAlgorithms
					.set(WifiConfiguration.AuthAlgorithm.SHARED);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
			config.allowedGroupCiphers
					.set(WifiConfiguration.GroupCipher.WEP104);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			config.wepTxKeyIndex = 0;
		}
		if (Type == 3) // WIFICIPHER_WPA
		{
			config.preSharedKey = "\"" + Password + "\"";
			config.hiddenSSID = true;
			config.allowedAuthAlgorithms
					.set(WifiConfiguration.AuthAlgorithm.OPEN);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			config.allowedPairwiseCiphers
					.set(WifiConfiguration.PairwiseCipher.TKIP);
			// config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			config.allowedPairwiseCiphers
					.set(WifiConfiguration.PairwiseCipher.CCMP);
			config.status = WifiConfiguration.Status.ENABLED;
		}
		return config;
	}

	public void setWifiAp(String mSSID, String mPasswd, boolean state) {
		Toast.makeText(FasongActivity.this,
				"正在" + (state ? "打开" : "关闭") + "热点", Toast.LENGTH_SHORT).show();
		if (wifiMng.isWifiEnabled()) {
			wifiMng.setWifiEnabled(false);
		}
		try {
			WifiConfiguration apConfiguration = new WifiConfiguration();
			apConfiguration.SSID = mSSID;
			apConfiguration.preSharedKey = mPasswd;
			apConfiguration.allowedKeyManagement
					.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			Method method = wifiMng.getClass().getMethod("setWifiApEnabled",
					WifiConfiguration.class, boolean.class);
			method.invoke(wifiMng, apConfiguration, state);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String getRealPathFromURI(Uri contentUri) {  
        String[] proj = {MediaStore.MediaColumns.DATA};  
        Cursor cursor=getContentResolver().query(contentUri, proj, null, null, null);  
        if(cursor.moveToNext()){  
            return cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));  
        }  
        cursor.close();  
        return null;  
    }  
	//无线热点状态读取
	private WIFI_AP_STATE getWifiApState() {
		int tmp;
		try {
			Method method = wifiMng.getClass().getMethod("getWifiApState");
			tmp = ((Integer) method.invoke(wifiMng));
			// Fix for Android 4
			if (tmp > 10) {
				tmp = tmp - 10;
			}
			return WIFI_AP_STATE.class.getEnumConstants()[tmp];
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return WIFI_AP_STATE.WIFI_AP_STATE_FAILED;
		}
	}

	public enum WIFI_AP_STATE {
		WIFI_AP_STATE_DISABLING, WIFI_AP_STATE_DISABLED, WIFI_AP_STATE_ENABLING, WIFI_AP_STATE_ENABLED, WIFI_AP_STATE_FAILED
	}
}
