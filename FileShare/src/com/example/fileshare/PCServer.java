package com.example.fileshare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Environment;
import android.os.Message;

public class PCServer extends Thread {
	private ServerSocket server = null;
	private int port = 30000;
	private Socket client;
	private InputStream is;
	private OutputStream os;
	private DataInputStream dis;
	private BufferedReader br;
	private BufferedWriter bw;
	private Timer timer;
	private String[] fileinfo;
	private byte[] buffer = new byte[FasongActivity.buffersize];
	private int sum = 0;
	private int readlen = 0;
	private long flen = 0;
	public static String objip;
	private int chongfu=1;

	public void stopServer() {
		try {
			server.close();
			System.out.println("服务器已关闭");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendProgress() {
		System.out.println("发送进度");
		Bundle data = new Bundle();
		Message msg = new Message();
		msg.what=1;
		msg.setData(data);
		data.clear();
		float tmp1 = sum;
		float tmp2 = flen;
		float jindu = tmp1 / tmp2;
		data.putString("jindu", "接收进度:"+fileinfo[0] + "-");
		data.putFloat("prges", jindu);
		PCActivity.hand.sendMessage(msg);
	}

	public void run() {
		try {
			if (server == null) {
				server = new ServerSocket(30000);
			}
			while (true) {
				System.out.println("服务已准备号");
				client = server.accept();
				objip = client.getInetAddress().getHostAddress();
				System.out.println("客户端连接成功，端口：30000");
				is = client.getInputStream();
				os = client.getOutputStream();
				dis = new DataInputStream(is);
				br = new BufferedReader(new InputStreamReader(is, "utf-8"));
				bw = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));
				while (true) {
					String cmd = br.readLine();
					if (cmd == null) {
						System.out.println("连接已断开");
						client.close();
						break;
					}
					System.out.println("收到命令：" + cmd);
					if (cmd.equals("end")) {
						System.out.println("文件全部接收完毕,关闭本次连接");
						client.close();
						break;
					}
					switch (cmd) {
					case "rf":
						System.out.println("进入接收文件状态");
						bw.write("rfs".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "rfe":
						System.out.println("进入发送文件状态");
						bw.write("rfes".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "fn":
						System.out.println("进入文件名接收阶段");
						String name = br.readLine();
						String filelen = br.readLine();
						fileinfo = new String[2];
						fileinfo[0] = name;
						fileinfo[1] = filelen;
						bw.write("fne".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "fr":
						sum = 0;
						readlen = 0;
						flen = Integer.parseInt(fileinfo[1]);
						File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/FileShare");
						if (!file.exists()) {
							file.mkdir();
						}
						File objf = new File(file.getAbsolutePath() + "/"
								+ fileinfo[0]);
						if (!objf.exists()) {
							objf.createNewFile();
							chongfu=1;
						}else{
							//有相同文件名
							objf=new File(file.getAbsolutePath()+"/"+"("+chongfu++ +")"+fileinfo[0]);
							while(objf.exists()){
								objf=new File(file.getAbsolutePath()+"/"+"("+chongfu++ +")"+fileinfo[0]);
							}
						}
						FileOutputStream fos = new FileOutputStream(objf);
						timer = new Timer();
						timer.schedule(new TimerTask() {

							public void run() {
								sendProgress();
							}
						}, 0, 800);
						while (sum < flen) {
							readlen = dis.read(buffer, 0, buffer.length);
							sum += readlen;
							fos.write(buffer, 0, readlen);
						}
						System.out.println(fileinfo[0] + "接收完成");
						sendProgress();
						fos.close();
						bw.write("fre".replaceAll("\n", "") + "\n");
						bw.flush();
						timer.cancel();
						timer.purge();
						break;
					}
					System.out.println("一个文件发送完成");
				}
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
