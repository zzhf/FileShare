package com.example.fileshare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Message;

public class ClientTerminal implements Runnable {
	private List<File> readyFile;
	private int port = 30000;
	private String ip = "192.168.43.1";
	private byte[] buffer = new byte[FasongActivity.buffersize];
	
	private int readlen = 0;
	private int sum = 0;
	private long filelen =0;
	private String filename;
	private Timer timer;

	public ClientTerminal(List<File> files) {
		readyFile = files;
	}
	public void sendProgress(){
		Bundle data=new Bundle();
		Message msg=new Message();
		msg.setData(data);
		data.clear();
		float tmp1=sum;
		float tmp2=filelen;
		float jindu=tmp1/tmp2;
		data.putString("jindu", filename+"-");
		data.putFloat("prges", jindu);
		FasongActivity.hand.sendMessage(msg);
	}
	public void run() {
		Socket client;
		if (readyFile.size() == 0) {
			System.out.println("没有文件可发送");
		} else {
			try {
				client = new Socket(ip, port);
				client.setTcpNoDelay(true);
				OutputStream os = client.getOutputStream();
				InputStream is = client.getInputStream();
				DataOutputStream dos = new DataOutputStream(os);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						is, "utf-8"));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						os, "utf-8"));
				while (true) {
					//发送文件名列表
					bw.write("fnl".replaceAll("\n", "") + "\n");
					bw.flush();
					int len = readyFile.size();
					for (int i = 0; i < len; i++) {
						bw.write(readyFile.get(i).getName()
								.replaceAll("\n", "")
								+ "\n");
						bw.flush();
						bw.write(readyFile.get(i).length() + "\n");
						System.out.println("名称:"+readyFile.get(i).getName()+"长度:" + readyFile.get(i).length());
						bw.flush();
					}
					bw.write("fnlte".replaceAll("\n", "") + "\n");
					bw.flush();
					String reponse = br.readLine();
					System.out.println("收到回复:" + reponse);
					//循环发送文件列表
					for (int i = 0; i < readyFile.size(); i++) {
						if(!readyFile.get(i).exists()){
							System.out.println("文件:"+readyFile.get(i).getName()+"不存在");
						}
						//发送文件接收命令
						System.out.println("发送第"+i+"个文件");
						bw.write("file".replaceAll("\n", "") + "\n");
						bw.flush();
						// 打开文件
						FileInputStream fis = new FileInputStream(
								readyFile.get(i));
						readlen = 0;
						sum = 0;
						filelen = readyFile.get(i).length();
						filename=readyFile.get(i).getName();
						timer=new Timer();
						timer.schedule(new TimerTask() {
							
							@Override
							public void run() {
								sendProgress();
							}
						},0,100);
						// 发送单个文件
						while (true) {
							readlen = fis.read(buffer, 0, buffer.length);
							if (readlen == -1) {
								fis.close();
								sendProgress();
								break;
							}
							dos.write(buffer, 0, readlen);
							dos.flush();
							sum = sum + readlen;
						}
						if (br.readLine().equals("fe")) {
							System.out.println("收到文件接收结束标志");
							timer.cancel();
							sendProgress();
						} else {
							System.out.println("接收端未回复结束标志");
							timer.cancel();
						}
					}
					bw.write("end".replaceAll("\n", "")+"\n");
					bw.flush();
					client.shutdownInput();
					client.shutdownOutput();
					client.close();
					break;
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("客户端文件发送完成");
		}
	}
}
