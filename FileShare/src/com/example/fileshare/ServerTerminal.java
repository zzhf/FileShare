package com.example.fileshare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Message;

public class ServerTerminal extends Thread {
	private int port = 30000;
	private int curfileid = 0;
	private static ServerSocket server;
	private List<String> files = new ArrayList<String>();
	private byte[] buffer = new byte[FasongActivity.buffersize];
	private String root;
	private Timer timer;
	private int readlen = 0;
	private int sum=0;
	private long filelen=0;
	private int chongfu=1;

	public ServerTerminal(String rootpath) {
		root=rootpath;
	}
	public void sendProgress(){
		System.out.println("更新进度");
		float tmp1=sum;
		float tmp2=filelen;
		float jindu=tmp1/tmp2;
		Bundle data=new Bundle();
		data.putString("wenzi", "接收进度:"+files.get(curfileid)+"-");
		data.putFloat("prges", jindu);
		Message msg=new Message();
		msg.what=1;
		msg.setData(data);
		JieshouActivity.hand.sendMessage(msg);
	}
	public void stopServer() {
		try {
			server.close();
			server = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			if (server == null) {
				server = new ServerSocket(port);
			}
			while (true) {
				System.out.println("准备建立连接");
				Socket sct = server.accept();
				OutputStream os = sct.getOutputStream();
				InputStream is = sct.getInputStream();
				DataInputStream dis = new DataInputStream(is);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						is, "utf-8"));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						os, "utf-8"));
				System.out.println("连接已准备就绪");
				curfileid = 0;
				while (true) {
					String cmd = br.readLine();
					if (cmd == null) {
						break;
					}
					System.out.println("收到命令:" + cmd);
					switch (cmd) {
					case "fnl":
						files.clear();
						System.out.println("准备接收文件信息");
						while (true) {
							String tmp = br.readLine();
							System.out.println("收到文件名："+tmp);
							if (tmp.equals("fnlte")) {
								System.out.println("文件名列表接受完成");
								break;
							}
							files.add(tmp);
						}
						System.out.println(files.toString());
						System.out.println("文件信息接收完成,共：" + files.size() / 2
								+ "个文件");
						bw.write("fne".replaceAll("\n", "") + "\n");
						bw.flush();
						//向接收界面发送文件名列表
						Bundle data=new Bundle();
						ArrayList<String> filestmp=new ArrayList<String>();
						for(int i=0;i<files.size();i++){
							if(i%2==0){
								filestmp.add(files.get(i));
							}
						}
						data.putStringArrayList("names", (ArrayList<String>) filestmp);
						Message msg=new Message();
						msg.what=0;
						msg.setData(data);
						JieshouActivity.hand.sendMessage(msg);
						break;
					case "file":
						System.out.println("进入文件接收段");
						System.out.println("存储卡根目录:"+root);
						File dir = new File(root+"/FileShare");
						if (!dir.exists()) {
							System.out.println("创建文件夹" + dir.mkdir());
						}
						File file = new File(root+"/FileShare/"
								+ files.get(curfileid));
						//判断文件是否存在
						if (!file.exists()) {
							System.out
									.println("文件创建结果:" + file.createNewFile());
							chongfu=1;
						}else{
							//有相同文件名
							file=new File(root+"/FileShare/"+"("+chongfu++ +")"+files.get(curfileid));
							while(file.exists()){
								file=new File(root+"/FileShare/"+"("+chongfu++ +")"+files.get(curfileid));
							}
						}
						FileOutputStream fos = new FileOutputStream(file);
						readlen = 0;
						String numstr = files.get(curfileid + 1);// 得到文件长度
						filelen = Long.parseLong(numstr);
						System.out.println("当前文件名:"+file.getName());
						System.out.println("当前文件长度:" + filelen);
						//设置定时器向接收界面发送进度
						timer=new Timer();
						timer.schedule(new TimerTask() {
							public void run() {
								sendProgress();
							}
						}, 0,500);
						sum = 0;
						// 循环读取
						while (sum < filelen) {
							readlen = dis.read(buffer, 0, buffer.length);
							if (readlen == -1) {
								System.out.println("文件"+files.get(curfileid)+"接收进度"+sum+"/"+filelen);
								System.out.println("网络连接断开");
								break;
							}
							sum += readlen;
							fos.write(buffer, 0, readlen);
						}
						System.out.println("一个文件接收完成");
						sendProgress();
						System.out.println("文件总长:" + filelen);
						curfileid += 2;
						bw.write("fe".replaceAll("\n", "") + "\n");
						bw.flush();
						fos.close();
						timer.cancel();
						timer.purge();
						break;
					default:
						break;
					}
					if (cmd.equals("end")) {
						sct.close();
						System.out.println("收到end指令,断开连接");
						break;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
