package com.example.fileshare;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class XuanzeActivity extends Activity {
	private TextView lujing;
	private ListView filelist;
	private Button queren, fanhui;
	private File currentFile;
	private ArrayList<File> selected, dqwj;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK){
			Intent intent=getIntent();
			Bundle data=new Bundle();
			data.putSerializable("selectedfile", new ArrayList<File>());
			intent.putExtras(data);
			XuanzeActivity.this.setResult(0, intent);
			XuanzeActivity.this.finish();
		}
		return false;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_xuanze);
		// 初始化
		lujing = (TextView) findViewById(R.id.xuanzeLujing);
		filelist = (ListView) findViewById(R.id.xuanzeFilelist);
		queren = (Button) findViewById(R.id.xuanzeQueren);
		fanhui = (Button) findViewById(R.id.xuanzeFanhui);
		selected = new ArrayList<File>();
		// 设置监听器
		queren.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent intent=getIntent();
				Bundle data=new Bundle();
				data.putSerializable("selectedfile", selected);
				intent.putExtras(data);
				XuanzeActivity.this.setResult(0, intent);
				XuanzeActivity.this.finish();
			}
		});
		fanhui.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent intent=getIntent();
				Bundle data=new Bundle();
				data.putSerializable("selectedfile", new ArrayList<File>());
				intent.putExtras(data);
				XuanzeActivity.this.setResult(0, intent);
				XuanzeActivity.this.finish();
			}
		});
		filelist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// 判断当前点击的是否为文件夹
				if (dqwj.get(arg2).isDirectory()) {
					// 当前文件时文件夹
					if (dqwj.get(arg2).list().length <= 0) {
						Toast.makeText(XuanzeActivity.this, "当前文件夹为空",
								Toast.LENGTH_SHORT).show();
					} else {
						setFileView(dqwj.get(arg2));
					}
				} else {
					// 当前点击的是文件
					if (selected.remove(dqwj.get(arg2))) {
						Toast.makeText(XuanzeActivity.this,
								"文件:" + dqwj.get(arg2).getName() + "已移除",
								Toast.LENGTH_SHORT).show();
					} else {
						selected.add(dqwj.get(arg2));
						Toast.makeText(XuanzeActivity.this,
								"文件:" + dqwj.get(arg2).getName() + "已添加",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		lujing.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				if (currentFile.getAbsoluteFile().equals(
						Environment.getExternalStorageDirectory()
								.getAbsoluteFile())) {
					return;
				} else {
					setFileView(currentFile.getParentFile());
				}
			}
		});
		// 初始化根目录
		File root = Environment.getExternalStorageDirectory();
		setFileView(root);
	}

	protected void setFileView(File objfile) {
		currentFile = objfile;
		ArrayList<File> mulu = new ArrayList<File>();
		ArrayList<File> wenjian = new ArrayList<File>();
		File[] currentFiles = currentFile.listFiles();
		//设置路径标签
		if (currentFile.getAbsolutePath().length() > 22) {
			lujing.setText("< " + ".../" + currentFile.getName());
		} else {
			lujing.setText("< " + currentFile.getAbsolutePath());
		}
		//区别文件夹和文件并设置当前文件列表
		for (int i = 0; i < currentFiles.length; i++) {
			if (currentFiles[i].isDirectory()) {
				// 当前文件是文件夹
				mulu.add(currentFiles[i]);
			} else {
				// 当前文件是文件
				wenjian.add(currentFiles[i]);
			}
		}
		mulu.addAll(wenjian);
		dqwj = mulu;
		//设置文件列表项
		int[] imgid={R.drawable.dir,R.drawable.file};
		List<Map<String,Object>> files=new ArrayList<Map<String,Object>>();
		for(int i=0;i<mulu.size();i++){
			File tmp=mulu.get(i);
			Map<String,Object> file=new HashMap<String, Object>();
			if(tmp.isDirectory()){
				file.put("typeimg", imgid[0]);
				file.put("filename", tmp.getName());
			}else{
				file.put("typeimg", imgid[1]);
				file.put("filename", tmp.getName());
			}
			files.add(file);
		}
		SimpleAdapter sa=new SimpleAdapter(XuanzeActivity.this, files,
				R.layout.filelist_item,
				new String[]{"typeimg","filename"},
				new int[]{R.id.typeimg,R.id.filename});
		filelist.setAdapter(sa);
	}
}