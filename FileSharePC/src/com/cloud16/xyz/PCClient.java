package com.cloud16.xyz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JLabel;

public class PCClient extends Thread {
	private String ip = "192.168.43.1";
	private ServerSocket server;
	private Socket client;
	private JButton sendbtn;
	private JLabel reslab;
	private InputStream is;
	private OutputStream os;
	private DataInputStream dis;
	private DataOutputStream dos;
	private BufferedReader br;
	private BufferedWriter bw;
	private boolean state = true;
	private Timer timer;
	private String[] fileinfo;
	private byte[] buffer = new byte[MainWin.buffersize];
	private int sum = 0;
	private int readlen = 0;
	private long flen = 0;
	private String objip;
	private String currentSendName;
	private int chongfu=1;

	public PCClient(JButton jbtn, JLabel lab) {
		sendbtn = jbtn;
		reslab = lab;
	}

	public void sendFiles(final List<File> files) {}

	public void stopClient() {
		try {
			if (server != null) {
				server.close();
				server = null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sendProgress(){
		float tmp1=sum;
		float tmp2=flen;
		float jindu=tmp1/tmp2;
		reslab.setText("接收进度:" + fileinfo[0] +"-"+Math.round(jindu*10000.0)/100+"%");
	}
	public void run() {
		try {
			if (server == null) {
				server = new ServerSocket(40000);
				System.out.println("服务器端创建完毕");
			}
			while (true) {
				System.out.println("服务已准备号");
				client = server.accept();
				objip = client.getInetAddress().getHostAddress();
				System.out.println("客户端连接成功，端口：40000");
				is = client.getInputStream();
				os = client.getOutputStream();
				dis = new DataInputStream(is);
				dos = new DataOutputStream(os);
				br = new BufferedReader(new InputStreamReader(is, "utf-8"));
				bw = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));
				while (true) {
					String cmd = br.readLine();
					if (cmd == null) {
						System.out.println("连接已断开");
						client.close();
						break;
					}
					System.out.println("收到命令：" + cmd);
					if (cmd.equals("end")) {
						System.out.println("文件全部接收完毕,关闭本次连接");
						client.close();
						break;
					}
					switch (cmd) {
					case "rf":
						state = false;
						System.out.println("进入接收文件状态");
						bw.write("rfs".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "rfe":
						state = true;
						System.out.println("进入发送文件状态");
						bw.write("rfes".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "fn":
						System.out.println("进入文件名接收阶段");
						String name = br.readLine();
						String filelen = br.readLine();
						fileinfo = new String[2];
						fileinfo[0] = name;
						fileinfo[1] = filelen;
						bw.write("fne".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					case "fr":
						sum = 0;
						readlen = 0;
						flen = Integer.parseInt(fileinfo[1]);
						File file = new File("FileShare");
						if (!file.exists()) {
							file.mkdir();
						}
						File objf = new File(file.getName() + "\\"
								+ fileinfo[0]);
						if (!objf.exists()) {
							objf.createNewFile();
							chongfu=1;
						}else{
							//有相同文件名
							objf=new File(file.getAbsolutePath()+"/"+"("+chongfu++ +")"+fileinfo[0]);
							while(objf.exists()){
								objf=new File(file.getAbsolutePath()+"/"+"("+chongfu++ +")"+fileinfo[0]);
							}
						}
						FileOutputStream fos = new FileOutputStream(objf);
						timer = new Timer();
						timer.schedule(new TimerTask() {

							public void run() {
								sendProgress();
							}
						}, 0, 500);
						while (sum < flen) {
							readlen = dis.read(buffer, 0, buffer.length);
							sum += readlen;
							fos.write(buffer, 0, readlen);
						}
						System.out.println(fileinfo[0] + "接收完成");
						sendProgress();
						fos.close();
						timer.cancel();
						timer.purge();
						bw.write("fre".replaceAll("\n", "") + "\n");
						bw.flush();
						break;
					}
				}
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
