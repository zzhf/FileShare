package com.cloud16.xyz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;

public class PCSend extends Thread {

	private byte[] buffer = new byte[MainWin.buffersize];
	private int sum = 0;
	private int readlen = 0;
	private long flen = 0;
	private List<File> files;
	private Timer timer;
	private String filename;
	private JLabel resultlabel;

	public PCSend(List<File> sendfile,JLabel jlab) {
		files = sendfile;
		resultlabel=jlab;
	}

	public void sendProgress() {
		float tmp1=sum;
		float tmp2=flen;
		float jindu=tmp1/tmp2;
		resultlabel.setText("发送进度:"+filename+"-"+Math.round(jindu*10000)/100+"%");
	}

	public void run() {
		try {
			// 新建Socket通讯发送文件，发送结束后关闭
			Socket mclient = new Socket("192.168.43.1", 30000);
			InputStream mis = mclient.getInputStream();
			OutputStream mos = mclient.getOutputStream();
			DataOutputStream mdos = new DataOutputStream(mos);
			BufferedReader mbr = new BufferedReader(new InputStreamReader(mis,
					"utf-8"));
			BufferedWriter mbw = new BufferedWriter(new OutputStreamWriter(mos,
					"utf-8"));
			mbw.write("rf".replaceAll("\n", "") + "\n");
			mbw.flush();
			String resp = mbr.readLine();
			if (resp == null) {
				System.out.println("接收文件指令回复出错");
				return;
			}
			if (resp.equals("rfs")) {
				System.out.println("发送传输文件指令成功");
				System.out.println(files.toString());
				for (int i = 0; i < files.size(); i++) {
					File ftmp = files.get(i);
					// 发送文件信息接收指令
					mbw.write("fn".replaceAll("\n", "") + "\n");
					mbw.flush();
					// 发送文件信息
					mbw.write(ftmp.getName().replaceAll("\n", "") + "\n");
					mbw.flush();
					mbw.write(Long.toString(ftmp.length()).replaceAll("\n", "")
							+ "\n");
					mbw.flush();
					resp = mbr.readLine();
					if (resp.equals("fne")) {
						System.out.println("接收到文件信息结束标志");
					} else {
						System.out.println("接收到文件信息结束标志");
					}
					// 发送文件
					mbw.write("fr".replaceAll("\n", "") + "\n");
					mbw.flush();
					sum = 0;
					readlen = 0;
					flen = ftmp.length();
					filename = ftmp.getName();
					FileInputStream fis = new FileInputStream(ftmp);
					timer = new Timer();
					timer.schedule(new TimerTask() {

						public void run() {
							sendProgress();
						}
					}, 0, 500);
					while (true) {
						readlen = fis.read(buffer, 0, buffer.length);
						if (readlen == -1) {
							System.out.println("文件:" + ftmp.getName() + "发送完成");
							System.out.println("收到回复：" + mbr.readLine());
							sendProgress();
							timer.cancel();
							fis.close();
							break;
						}
						sum += readlen;
						mdos.write(buffer, 0, readlen);
						mdos.flush();
					}
				}
				mbw.write("end".replaceAll("\n", "") + "\n");
				mbw.flush();
				timer.cancel();
				mclient.close();
			} else {
				System.out.println("收到未知指令回复:" + resp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
