package com.cloud16.xyz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;


public class MainWin {
	public static int buffersize=1024*2;
	private List<File> sendFiles=new ArrayList<File>();
	private PCClient connection=null;
	private JFrame frmFilesharepc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWin window = new MainWin();
					window.frmFilesharepc.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFilesharepc = new JFrame();
		frmFilesharepc.setResizable(false);
		frmFilesharepc.setTitle("FileSharePC");
		frmFilesharepc.getContentPane().setBackground(Color.LIGHT_GRAY);
		frmFilesharepc.setBounds(100, 100, 600, 470);
		frmFilesharepc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFilesharepc.getContentPane().setLayout(null);
		//进度显示标签
		final JLabel reslabel = new JLabel("\u53D1\u9001\u8FDB\u5EA6");
		reslabel.setBackground(Color.GRAY);
		reslabel.setFont(new Font("宋体", Font.PLAIN, 28));
		reslabel.setBounds(0, 0, 594, 38);
		frmFilesharepc.getContentPane().add(reslabel);
		
		final JLabel recvlabel = new JLabel("\u63A5\u6536\u8FDB\u5EA6");
		recvlabel.setFont(new Font("宋体", Font.PLAIN, 28));
		recvlabel.setBounds(0, 37, 594, 47);
		frmFilesharepc.getContentPane().add(recvlabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 91, 594, 288);
		frmFilesharepc.getContentPane().add(scrollPane);
		
		final JList<String> list = new JList<String>();
		list.setBackground(Color.WHITE);
		scrollPane.setViewportView(list);
		
		final JButton fasong = new JButton("\u53D1\u9001\u6587\u4EF6");
		fasong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PCSend send=new PCSend(sendFiles,reslabel);
				send.start();
			}
		});
		fasong.setFont(new Font("宋体", Font.PLAIN, 28));
		fasong.setBounds(0, 383, 155, 47);
		frmFilesharepc.getContentPane().add(fasong);
		
		JButton xuanze = new JButton("\u9009\u62E9\u6587\u4EF6");
		xuanze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("点击了选择文件按钮");
				JFileChooser jfc=new JFileChooser("C:");
				jfc.setMultiSelectionEnabled(true);
				jfc.showOpenDialog(frmFilesharepc);
				File[] files=jfc.getSelectedFiles();
				Vector<String> filenames=new Vector<String>();
				for(int i=0;i<files.length;i++){
					System.out.println(files[i].getName());
					filenames.add(files[i].getName());
					sendFiles.add(files[i]);
				}
				list.setListData(filenames);
			}
		});
		xuanze.setFont(new Font("宋体", Font.PLAIN, 28));
		xuanze.setBounds(439, 383, 155, 47);
		frmFilesharepc.getContentPane().add(xuanze);
		
		final JButton jianli = new JButton("\u5EFA\u7ACB\u8FDE\u63A5");
		jianli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(connection==null){
					//创建连接
					//发送问候消息
					new Thread(){
						public void run() {
							try {
								Socket woshou=new Socket("192.168.43.1",30000);
								BufferedWriter woshoubw=new BufferedWriter(new OutputStreamWriter(woshou.getOutputStream(),"utf-8"));
								woshoubw.write("end".replaceAll("\n", "")+"\n");
								woshoubw.flush();
								woshou.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}.start();
					connection=new PCClient(fasong, recvlabel);
					connection.start();
					jianli.setText("关闭连接");
				}else{
					//关闭连接
					connection.stopClient();
					connection=null;
					System.out.println("连接已关闭");
					jianli.setText("建立连接");
				}
			}
		});
		jianli.setFont(new Font("宋体", Font.PLAIN, 28));
		jianli.setBounds(218, 383, 163, 47);
		frmFilesharepc.getContentPane().add(jianli);
	}
}
